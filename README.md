# Identicon

An Elixir library for generating GitHub-like symmetrical 5x5 identicons.


## Installation

To run Identicon you must have Elixir and Erlang installed on your machine.

macOS - install via Homebrew:
```
> brew install elixir
> git clone https://gitlab.com/colonelrascals/identicon.git
> cd ./indenticon
> mix deps.get
```

## Usage

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
